const mysql = require("mysql");
const express = require("express");
const bodyParser = require("body-parser");

//Initializing server
const app = express();
app.use(bodyParser.json());
const port = 8080;
app.listen(port, () => {
    console.log("Server online on: " + port);
});

app.use('/' , express.static('../front-end'))

const connection = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "siscit_abonament"
});

connection.connect(function (err) {

    console.log("Connected to database!");
    const sql = "CREATE TABLE IF NOT EXISTS abonament(nume VARCHAR(40),prenume VARCHAR(40),varsta VARCHAR(3),cnp VARCHAR(20),telefon VARCHAR(15),email VARCHAR(50),facebook VARCHAR(60),tipAbonament VARCHAR(20),nrCard VARCHAR(40),cvv INTEGER, sex VARCHAR(1))";
    connection.query(sql, function(err, result) {
        if (err) throw err;
      });
});

app.post("/abonament", (req, res) => {
    let nume = req.body.nume;
    let prenume = req.body.prenume;
    let varsta = req.body.varsta;
    let cnp = req.body.cnp;
    let telefon = req.body.telefon;
    let email = req.body.email;
    let facebook = req.body.facebook;
    let tipAbonament = req.body.tipAbonament;
    let nrCard = req.body.nrCard;
    let cvv = req.body.cvv;
    let error = [];
    let sex = 'N';

    if(cnp[0]=='1'||cnp[0]=='3'||cnp[0]=='5'){
         sex = 'M';
    }
    else if(cnp[0]=='2'||cnp[0]=='4'||cnp[0]=='6'){
         sex = 'F';
    }

    const data_curenta = new Date();
    const an_curent = data_curenta.getFullYear() * 1;
    const luna_curenta = data_curenta.getMonth() * 1;
    const zi_curenta = data_curenta.getDay() * 1;
    const luna_cnp = cnp[3] * 10 + cnp[4]*1 - 1;
    var an_cnp = 0;
    if(cnp[1] == '0' || cnp[1] == '1'){
         an_cnp = 2000 + (cnp[1]*10) + cnp[2]*1;
        console.log(an_cnp);
    }
    else{
         an_cnp = 1900 + (cnp[1]*10) + cnp[2]*1;
        console.log(an_cnp);
    }
    const zi_cnp = cnp[5] * 10 + cnp[6] * 1;
    if(luna_curenta < luna_cnp){
        if((an_curent - an_cnp) != (varsta - 1)){
            error.push("Varsta nu corespunde cu CNP-ul.");
            console.log("Varsta nu corespunde cu CNP-ul.");
        }
            
        
    }
    if(luna_curenta > luna_cnp){
        if((an_curent - an_cnp) != varsta){
            error.push("Varsta nu corespunde cu CNP-ul.");
            console.log("Varsta nu corespunde cu CNP-ul.");
        }
            
        
    }
    if(luna_curenta == luna_cnp){
        if(zi_curenta < zi_cnp){
            if((an_curent - an_cnp) != (varsta - 1)){
                error.push("Varsta nu corespunde cu CNP-ul.");
                console.log("Varsta nu corespunde cu CNP-ul.");
            }
        }
        else{
            if((an_curent - an_cnp) != varsta){
                error.push("Varsta nu corespunde cu CNP-ul.");
                console.log("Varsta nu corespunde cu CNP-ul.");
            }
        }
    }

    if(!nume||!prenume||!varsta||!cnp||!telefon||!email||!facebook||!tipAbonament||!nrCard||!cvv||(sex=='N'))
    {
        if(!nume){
            error.push("Numele nu a fost introdus.");
            console.log("Numele nu a fost introdus.");
        }
        if(!prenume){
            error.push("Prenumele nu a fost introdus.");
            console.log("Prenumele nu a fost introdus.");
        }
        if(!varsta){
            error.push("Varsta nu a fost introdusa.");
            console.log("Varsta nu a fost introdusa.");
        }
        if(!cnp){
            error.push("CNP-ul nu a fost introdus.");
            console.log("CNP-ul nu a fost introdus.");
        }
        if(!telefon){
            error.push("Telefonul nu a fost introdus.");
            console.log("Telefonul nu a fost introdus.");
        }
        if(!email){
            error.push("Adresa de email nu a fost introdusa.");
            console.log("Adresa de email nu a fost introdusa.");
        }
        if(!facebook){
            error.push("Linkul catre pagina de Facebook nu a fost introdus.");
            console.log("Linkul catre pagina de Facebook nu a fost introdus.");
        }
        if(!tipAbonament){
            error.push("Tipul de abonament nu a fost introdus.");
            console.log("Tipul de abonament nu a fost introdus.");
        }
        if(!nrCard){
            error.push("Numarul cardului nu a fost introdus.");
            console.log("Numarul cardului nu a fost introdus.");
        }
        if(!cvv){
            error.push("CVV-ul cardului nu a fost introdus.");
            console.log("CVV-ul cardului nu a fost introdus.");
        }
        if(sex=='N'){
            error.push("Prima cifra a CNP-ului nu este valida.");
            console.log("Prima cifra a CNP-ului nu este valida.");
        }
    }
    else{
        if(!nume.match("^[A-Za-z]+$")){
            error.push("Numele trebuie sa contina doar litere mari si mici.");
            console.log("Numele trebuie sa contina doar litere mari si mici.");
        }
        else if(nume.length < 3 || nume.length > 20){
            error.push("Numele trebuie sa aiba intre 3 si 20 de caractere");
            console.log("Numele trebuie sa aiba intre 3 si 20 de caractere");
        }
        if(!prenume.match("^[A-Za-z]+$")){
            error.push("Prenumele trebuie sa contina doar litere mari si mici.");
            console.log("Prenumele trebuie sa contina doar litere mari si mici.");
        }
        else if(prenume.length < 3 || prenume.length > 20){
            error.push("Prenumele trebuie sa aiba intre 3 si 20 de caractere");
            console.log("Prenumele trebuie sa aiba intre 3 si 20 de caractere");
        }
        if(varsta.length > 3 || !varsta.match("^[0-9]+$")){
            error.push("Varsta trebuie sa contina doar cifre si sa aiba lungimea intre 1 si 3 caractere");
            console.log("Varsta trebuie sa contina doar cifre si sa aiba lungimea intre 1 si 3 caractere");
        }
        if(cnp.length != 13 || !cnp.match("^[0-9]+$")){
            error.push("CNP-ul trebuie sa contina doar cifre si sa aiba lungimea de 13 caractere");
            console.log("CNP-ul trebuie sa contina doar cifre si sa aiba lungimea de 13 caractere");
        }
        if(telefon.length != 10 || !telefon.match("^[0-9]+$")){
            error.push("Numarul de telefon trebuie sa contina doar cifre si sa aiba lungimea 10");
            console.log("Numarul de telefon trebuie sa contina doar cifre si sa aiba lungimea 10");
        }
        if(!email.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)){
            error.push("Emailul trebuie sa fie de forma exemplu@zzz.yyyy");
            console.log("Emailul trebuie sa fie de forma exemplu@zzz.yyyy");
        }
        if(!facebook.match('(?:http:\/\/)?(?:www.)?facebook.com\/(?:(?:\w)*#!\/)?(?:pages\/)?(?:[?\w\-]*\/)?(?:profile.php\?id=(\d.*))?([\w\-]*)?')){
            error.push("Link invalid de facebook");
            console.log("Link invalid de facebook");
        }
        if(nrCard.length != 16 || !nrCard.match("^[0-9]+$")){
            error.push("Lungimea numarului cardului trebuie sa fie 16 si sa contina doar cifre");
            console.log("Lungimea numarului cardului trebuie sa fie 16 si sa contina doar cifre");
        }
        if(cvv.length != 3 || !cvv.match("^[0-9]+$")){
            error.push("Lungimea CVV-ului trebuie sa fie 3 si sa contina doar cifre");
            console.log("Lungimea CVV-ului trebuie sa fie 3 si sa contina doar cifre");
        }
        connection.query("SELECT COUNT(*) as nrmail FROM abonament WHERE email = '"+email+"'", function (err, result) {
            if(err) throw err;
            if(result[0].nrmail!=0){
                error.push("emailul deja exista");
                console.log("emailul deja exista");
            }
        });
    }

    if (error.length === 0) {
        const sql =
            "INSERT INTO abonament (nume,prenume,varsta,cnp,telefon,email,facebook,tipAbonament,nrCard,cvv,sex) VALUES('" +nume +"','" +prenume +"','"+varsta +"','"+cnp +"','" +telefon +"','" +email +"','" +facebook +"','" + tipAbonament +"','" + nrCard +"','" + cvv+"','" +sex +"')";
        connection.query(sql, function (err, result) {
            if (err) throw err;
            console.log("Abonament achizitionat!");
        });

        res.status(200).send({
            message: "Abonament achizitionat!"
        });
        console.log(sql);
    } else {
        res.status(500).send(error)
        console.log("Eroare la inserarea in baza de date!");
    }
});